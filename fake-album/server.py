# Fake Album - Campus Collaborative Challenge
# Princeton University
#
# Example:
#   sudo python server.py

from flask import Flask, request, make_response, send_file
from getstuff import getImage, getTitle, getQuote
from generate_cover import generate
import tempfile
from random import randint

from PIL import Image

app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello World!"

def getRandomImage():
    import urllib2 as urllib
    resp = urllib.urlopen('http://beesbuzz.biz/crap/flrig.cgi')
    s = resp.read()
    tag = 'http://www.flickr.com/photos/'
    start = s.index(tag) + len(tag)
    end = s.index('"', start)
    url = tag + s[start : end]
    return url

@app.route("/fakealbum")
def fakealbum():
    try:
        name_url = request.args['name']
    except KeyError:
        name_url = 'http://en.wikipedia.org/wiki/Special:Random'
        print name_url
    try:
        quote_url = request.args['quote']
    except KeyError:
        r = randint(1,5200)
        quote_url = 'http://www.quotationspage.com/quote/%d.html'%r
        print quote_url
    try:
        image_url = request.args['image']
    except KeyError:
        image_url = getRandomImage()
        print image_url

    # Fetch stuff
    image = getImage(image_url)
    title = getTitle(name_url)
    quote = getQuote(quote_url)

    # Generate Album Cover
    album_cover = generate(image, title, quote);

    # Make a temporary file
    tmpimage = tempfile.NamedTemporaryFile(mode='rw+b', suffix='jpg')
    fname = tmpimage.name
    album_cover.save(fname, "JPEG")

    # Send that temporary file
    return send_file(tmpimage, mimetype='image/jpeg')

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80, debug=True)
