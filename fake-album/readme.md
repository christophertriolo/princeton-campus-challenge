Fake Album
==========

[Promblem Information](https://campus.interviewstreet.com/challenges/dashboard/#problem/4f807efb36541)

Team Members
------------
Christopher Triolo
Emily Lancaster
Harvest Zhang
Edward Zhang
Alice Zhang
Katherine Li


Setup Instructions
------------------
Install python
Install Flask (sudo pip install flask)
Install PIL (http://effbot.org/downloads/Imaging-1.1.7.tar.gz)
    Unzip
    cd to unzipped directory 
    python setup.py install
Install BeautifulSoup
