import math, random
from PIL import ImageFont, ImageDraw, ImageOps, Image

SIZE = 400
MARGIN = 10

FONTS = {
    'homestead': 'fonts/Homestead-Display.ttf',
    'ninja':     'fonts/soul_ninja.ttf',
    'moki':      'fonts/Moki-Mono.otf',
    'xxx':    'fonts/XPED_Distressed.ttf'
}


LEFT = 'LEFT'
RIGHT = 'RIGHT'
CENTER = 'CENTER'

TOP = 'TOP'
BOTTOM = 'BOTTOM'
MIDDLE = 'MIDDLE'

BOUNDS = {
    'TOP_LEFT':      ((  0,  0), (200,200), (LEFT,   TOP)),
    'TOP_CENTER':    ((100,  0), (200,200), (CENTER, TOP)),
    'TOP_RIGHT':     ((200,  0), (200,200), (RIGHT,  TOP)),
    'MIDDLE_LEFT':   ((  0,100), (200,200), (LEFT,   MIDDLE)),
    'MIDDLE_CENTER': ((100,100), (200,200), (CENTER, MIDDLE)),
    'MIDDLE_RIGHT':  ((200,100), (200,200), (RIGHT,  MIDDLE)),
    'BOTTOM_LEFT':   ((  0,200), (200,200), (LEFT,   BOTTOM)),
    'BOTTOM_CENTER': ((100,200), (200,200), (CENTER, BOTTOM)),
    'BOTTOM_RIGHT':  ((200,200), (200,200), (RIGHT,  BOTTOM)),
    'TOP_HALF':      ((  0,  0), (400,200), (CENTER, TOP)),
    'BOTTOM_HALF':   ((  0,200), (400,200), (CENTER, BOTTOM)),
}

LAYOUTS = [
    ('TOP_LEFT',    'BOTTOM_RIGHT'),
    ('TOP_CENTER',  'BOTTOM_CENTER'),
    ('TOP_RIGHT',   'BOTTOM_LEFT'),
    ('TOP_LEFT',    'TOP_RIGHT'),
    ('MIDDLE_LEFT', 'MIDDLE_RIGHT'),
    ('BOTTOM_LEFT', 'BOTTOM_RIGHT'),
    ('TOP_HALF',    'BOTTOM_HALF'),
]


def getFont(size=72, name=None):
    if name == None:
        rand = random.randint(0, len(FONTS.keys()))
        font_name = random.choice(list(FONTS.keys()))
        name = FONTS[font_name]
    return (name, ImageFont.truetype(name, size))

def getTitle(image, title):
    font = getFont(72)
    txt = Image.new('L', font.getsize())

def isGoodWidth(trial, want, isHigh):
    if isHigh and trial <= want: return True
    return want >= trial - MARGIN and want <= trial

def getFontWithWidth(text, width, low=20, high=72):
    place = high
    last_good = None
    rand = None
    while low < high:
        (rand, font) = getFont(place, rand)
        (w, h) = font.getsize(text)
        if isGoodWidth(w, width, place == high):
            return font
        elif w > width:
            high = place
        elif w < width:
            low = place + 1
        if w <= width: last_good = font
        place = (low + high) / 2
    return last_good

# Breaks the code in half
def splitToWrap(text, times):
    if times == 1: return [ text ]
    parts = text.split(' ')
    stop = 0; currLen = len(parts[stop]); s = ''
    while currLen < (len(text) / times):
        stop += 1
        currLen += 1 + len(parts[stop])
    stop += 1
    first = ' '.join(parts[:stop]).strip()
    sec = ' '.join(parts[stop:]).strip()
    sec = splitToWrap(sec, times - 1)
    return sec + [ first ]

def getLayout(image):
    min_layout = ();
    min_contrast = 1000000000;

    for layout in LAYOUTS:
        c = max(contrast(image, BOUNDS[layout[0]]), contrast(image, BOUNDS[layout[1]]))
        if (min_contrast > c):
            min_layout = layout
            min_contrast = c

    return min_layout


def getTextLocation(thisSize, topLeft, boxSize, aligns, lastLoc, lastSize):
    align = aligns[1]
    if align == MIDDLE:
        yPosition = topLeft[1] + (boxSize[1] - thisSize[1]) / 2 + lastLoc[1] + lastSize[1]
    elif align == TOP:
        yPosition = topLeft[1] + lastSize[1] + lastLoc[1]
    else:
        yPosition = (topLeft[1] + boxSize[1]) - thisSize[1] - lastLoc[1] - lastSize[1]

    align = aligns[0]
    xPosition = topLeft[0]
    if align == RIGHT:
        xPosition += boxSize[0] - thisSize[0] - MARGIN * 2
    elif align == CENTER:
        xPosition += ((boxSize[0] - thisSize[0]) / 2)
    return (xPosition, yPosition)

def getTextDrawingInfo(image, text, boundingBox):
    (topLeft, boxSize, aligns) = boundingBox
    # Font
    parts = [text]
    amount = boxSize[0] - MARGIN * 3
    f = getFontWithWidth(text, amount)
    splits = 2
    while f == None:
        parts = splitToWrap(text, splits)
#        if not bottom: parts.reverse()
        longer = reduce(lambda x, y: x if len(x) > len(y) else y, parts)
        f = getFontWithWidth(longer, amount)
        splits += 1

    # More stuff
    #offset = 0
    toDraw = []
    #newWidth = 0
    lastLoc = (0, 0); lastSize = (0, 0)
    for part in parts:
        sizes = f.getsize(part)
        txt = Image.new('L', (400, 400))
        d = ImageDraw.Draw(txt)
        d.text( (MARGIN, 0), part, font=f, fill=255)

        # Get the location and paste
        location = getTextLocation(sizes, topLeft, boxSize, aligns,
                                   lastLoc, lastSize)
        """
        if not bottom:
            location = (0, MARGIN + offset)
        else:
            location = (0, SIZE - MARGIN - sizes[1] - offset)
        offset += sizes[1]
        newWidth = max(newWidth, sizes[0])
        """
        lastLoc = location
        lastSize = sizes
        toDraw.append( (txt, location, sizes) )

    # Find a color and return
    #colorTuple = choosecolor(image, (newWidth, offset), lastLoc)
    colorTuple = choosecolor(image, boxSize, topLeft)
    return (toDraw, colorTuple)
    # w=txt.rotate(17.5,  expand=1)

# Draws all of the texts
def drawAll(image, toDraw):
    for drawInfo in toDraw:
        (drawings, colorTuple) = drawInfo
        for drawing in drawings:
            (txt, location, sizes) = drawing
            color = ImageOps.colorize(txt, colorTuple, colorTuple)
            image.paste(color, location, txt)

def generate(image, title, quote):
    print title
    print quote
    image = crop(image)
    #image = image.filter(ImageFilter.FIND_EDGES)

    # Set up bounding box...
    topLeft = (0, 0)
    botLeft = (0, 0)
    boxSize = (SIZE, SIZE)

    # get layout
    layout = getLayout(image)

    # Draw
    draws = []
    draws.append(getTextDrawingInfo(image, title, BOUNDS[layout[0]]))
    draws.append(getTextDrawingInfo(image, quote, BOUNDS[layout[1]]))
    drawAll(image, draws)
    return image

def crop(image, align=CENTER):
    t = image.size
    scale = 400./min(t[0],t[1])
    image = image.resize((int(t[0]*scale),int(t[1]*scale)), Image.ANTIALIAS)
    if align == CENTER:
        t = image.size
        x = (t[0] - 400)/2
        y = (t[1] - 400)/2
    elif align == LEFT:
        x = 0
        y = 0
    else:
        t = image.size
        x = t[0]-400
        y = t[1]-400
    return image.crop((x,y,x+400,y+400))

def choosecolor(image, d, p):
    p = image.crop((p[0], p[1], p[0]+d[0], p[1]+d[1])).load()
    w, h = d
    if not w or not h:
        return (255,255,255)
    lum = 0.
    dev = 0.
    maxl = 0.
    minl = 1.
    for x in range(w):
        for y in range(h):
            clum = (0.3*p[x,y][0] + 0.59*p[x,y][1] + 0.11*p[x,y][2])/255
            avg = (p[x,y][0] + p[x,y][1] + p[x,y][2])/3
            cdev = (p[x,y][0] - avg)*(p[x,y][0] - avg) + (p[x,y][1] - avg)*(p[x,y][1] - avg) + (p[x,y][2] - avg)*(p[x,y][2] - avg)
            dev += cdev
            lum += clum
            if clum > maxl:
                maxl = clum
            if clum < minl:
                minl = clum
    lum /= w*h
    dev /= w*h
    if maxl - minl > .7 and dev < 200:
        if lum > .5:
            return (100,100,255)
        else:
            return (255,0,0)
    if lum > .5:
        return (0,0,0)
    else:
        return (255,255,255)


def luminance(image):
    lum = []
    hist = image.histogram()
    r = g = b = 0;
    num = image.size[0] * image.size[1];

    for i in range(255):
        r += hist[i] * i;
        g += hist[i+255] * i;
        b += hist[i+255*2] * i;

    r = r/num;
    g = g/num;
    b = b/num;

    return r*0.3 + g*0.6 + b*0.1

def contrast(image, bound):
    num = bound[1][0] * bound[1][1];
    sum_x = 0;
    sum_x2 = 0;

    for i in range(bound[1][0]):
        for j in range(bound[1][1]):
            pixel = image.getpixel((i+bound[0][0], j+bound[0][1]))
            lum = pixel[0]*0.3 + pixel[1]*0.6 + pixel[2]*0.1
            sum_x += lum
            sum_x2 += lum * lum

    mean = sum_x / num
    stddev = math.sqrt((sum_x2/num) - (mean * mean))
    return stddev

if __name__ == "__main__":
    image_dark=crop(Image.open("01-dark.jpg"))
    image_contrast=crop(Image.open("02-contrast.jpg"))
    image_monotone=crop(Image.open("03-monotone.jpg"))
    title='Hello, world!'
    quote='Something profound.'
    new_image = generate(image_dark, title, quote);
    new_image.save('test2.jpeg');
    print "{}'s Luminance:{}".format('Dark', luminance(image_dark))
    print "{}'s Luminance:{}".format('Contrast', luminance(image_contrast))
    print "{}'s Luminance:{}".format('Monotone', luminance(image_monotone))
    i = crop(image_contrast)
    print "{}'s Top Left:{} Top_Right:{} Bottom_Left:{} Bottom_Right:{}".format('Contrast',
                                                                                contrast(i, BOUNDS['TOP_LEFT']),
                                                                                contrast(i, BOUNDS['TOP_RIGHT']),
                                                                                contrast(i, BOUNDS['BOTTOM_LEFT']),
                                                                                contrast(i, BOUNDS['BOTTOM_RIGHT']));
    print getLayout(i)
    i.save('cropped.jpg')
