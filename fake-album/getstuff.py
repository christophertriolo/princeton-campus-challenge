import urllib2 as urllib
from PIL import Image
import re, unicodedata
from cStringIO import StringIO
from BeautifulSoup import BeautifulSoup

def getImage(url):
    flickr_page = BeautifulSoup(urllib.urlopen(url))
    image_url = flickr_page.find('meta', {'property': 'og:image'})['content']
    image = Image.open(StringIO(urllib.urlopen(image_url).read()))
    return image

def getTitle(url):
    opener = urllib.build_opener()
    opener.addheaders = [('User-agent', 'Mozilla/5.0')]
    img_file = opener.open(url)
    im = img_file.read()

    # Extract what's in the title
    tags = ["<title>", "- Wikipedia", "List of"]
    start = im.index(tags[0]) + len(tags[0])
    end = im.index(tags[1], start)
    happy = im[start:end]

    # Kill anything in parenthesis or with "List of"
    happy = re.sub('\(.*\)', '', happy).strip()
    if happy.startswith(tags[2]): happy = happy[len(tags[2]):]
    if isinstance(happy, unicode):
        print happy
        happy = unicodedata.normalize('NFKD', happy).encode('ascii','ignore')
    return alphanumonly(happy.strip())

def alphanumonly(s):
    return re.sub(r"[^\w\s']", '', s)

def simplify(s):
    #conj = [w.strip() for w in open("conjunctions.txt",'r').readlines()]
    #conj += 'for and nor but or yet so is was am are were'.split()
    conj = 'and nor but or yet so while'.split()
    ret = ''
    retlist = []
    word = 0
    reset = 0
    for w in s.split():
        if reset and w == 'not':
            continue
        if w in conj:
            if word >= 2:
                retlist.append(ret)
                ret = ''
            else:
                ret = ''
                word = 0
                reset = 1
        else:
            ret += w + " "
            word += 1
    if word >= 2:
        retlist.append(ret)
        return retlist
    if retlist:
        return retlist
    else:
        return [s]

def getQuote(url):
    f = urllib.urlopen(url)
    soup = BeautifulSoup(f)
    quotenode = soup.find('blockquote', {'class':'quotebig'})
    quote = quotenode.find('dt')
    sentences = re.split('[!?,.;:()-]', quote.contents[0])
    ch = []
    for s in sentences:
        if len(s) >= 4 and len(s.split()) > 1:
            print s
            # Prefer titles starting with 'a' or 'the'
            ind = None
            for ind in re.finditer('\Wthe\W',s):
                pass
            if ind:
                s = s[ind.start():]
            else:
                for ind in re.finditer('\Wa\W',s):
                    pass
                if ind:
                    s = s[ind.start():]
                else:
                    for ind in re.finditer('\Wan\W',s):
                        pass
                    if ind:
                        s = s[ind.start():]
            if len(s.split()) > 4:
                ch += [alphanumonly(simplified) for simplified in simplify(s)]
            else:
                ch.append(s)
    last = ''
    resort = ''
    if not ch:
        tmp = ''
        # Last resort: Two long words
        for w in quote.contents[0].split():
            if len(w) < 5:
                tmp = ''
                last = w
            elif tmp == '':
                tmp += w
            else: 
                tmp += ' ' + w
                resort = tmp
                break
        if len(last) < 4 and last:
            ch.append(last + ' '  + resort)
        else:
            ch.append(resort)
    if ch:
        lengths = [99999, 6, 4, 1, 2, 3, 5]
        lengths += range(6, 40)
        ch = sorted(ch, key=lambda s:lengths[len(s.split())])
        return ch[0].strip()
    else:
        return quote.contents[0].strip()

if __name__ == "__main__":
    title = getTitle('http://en.wikipedia.org/wiki/Jewish_cuisine');
    print "Title: %s" % title

    quote = getQuote('http://www.quotationspage.com/quote/748.html');
    print "Quote: %s" % quote

    image = getImage('http://www.flickr.com/photos/weedobooty/7031679837/');
    image.save('test.jpeg');
