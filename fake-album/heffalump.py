from PIL import Image
import generate_image
import sys

def main(filename, title, artist):
    im = Image.open(filename)
    generate_image(im, title, artist)
    
    # Insert other function calls here -- remember to import at top :)



if __name__ == '__main__':
    if len(sys.argv) < 4:
        print 'Usage: python heffalump "filename" "title" "artist"'
    main(sys.argv[1], sys.argv[2], sys.argv[3])
    
